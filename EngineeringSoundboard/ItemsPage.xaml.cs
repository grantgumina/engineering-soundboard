﻿using Microsoft.Advertising.WinRT.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Items Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234233

namespace EngineeringSoundboard
{
    /// <summary>
    /// A page that displays a collection of item previews.  In the Split Application this page
    /// is used to display and select one of the available groups.
    /// </summary>
    public sealed partial class ItemsPage : EngineeringSoundboard.Common.LayoutAwarePage
    {
        public ItemsPage()
        {
            this.InitializeComponent();
        }
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        // AD ERROR HANDLING
        private void OnAdError(object sender, AdErrorEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("AdControl error (" + ((AdControl)sender).Name + "): " + e.Error + " ErrorCode: " + e.ErrorCode.ToString());
        }

        private void or1Btn_Click(object sender, RoutedEventArgs e)
        {
            player.Source = new Uri(this.BaseUri, "Assets/3stepprocess.mp3");
            player.Position = TimeSpan.Zero;
            player.Play();
        }

        private void or2Btn_Click(object sender, RoutedEventArgs e)
        {
            player.Source = new Uri(this.BaseUri, "Assets/identifytheproblem.mp3");
            player.Position = TimeSpan.Zero;
            player.Play();
        }
    }
}
